<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', 'TableController@showStartform');
Route::post('/', 'TableController@showStartform');

Route::get('table', 'TableController@showTable');
Route::post('table', 'TableController@showTable');

Route::get('setup', 'TableController@startnames');
Route::post('setup', 'TableController@startnames');
