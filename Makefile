.DEFAULT_GOAL := help

.PHONY: build
build: ## Build the application image
	docker-compose build

.PHONY: start
start: ## Start the application
	docker-compose up -d

.PHONY: install
install: ## Install the application
	$(MAKE) update

.PHONY: update
update: ## Update the application after a build
	docker-compose run --rm app post_update.sh

.PHONY: stop
stop: ## Stop the application
	docker-compose down --remove-orphans

## Dev related tasks
.PHONY: composer-install
composer-install: ## Ensure all PHP dependencies are present on the host machine
	docker run --env-file .env.example --rm -t -v ${CURDIR}:/app composer:1.6.5 install

## Dev related tasks
.PHONY: dev-permissions
dev-permissions: ## Ensure correct chmod on local machine
	sudo chmod 777 -R storage/
    sudo chmod 777 -R bootstrap/cache

.PHONY: composer-update
composer-update: ## Ensure all PHP dependencies are present on the host machine
	docker run --env-file .env.example --rm -t -v ${CURDIR}:/app composer:1.6.5 update --prefer-dist

.PHONY: help
help: ## Displays this help
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-16s\033[0m %s\n", $$1, $$2}'
