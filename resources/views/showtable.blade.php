@extends('layouts.master')

@section('content')
<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
    <h1 class="page-header">Punkte grafisch</h1>

    <div class="row placeholders">
        <div class="col-xs-6 col-sm-3 placeholder">
            <div id="canvas-holder">
                <canvas id="pointpiechart" width="200" height="200"/>
            </div>
            <h4>Punkteverteilung</h4>
            <span class="text-muted">Wer hat wie viele Punkte?</span>
        </div>
        <div class="col-xs-6 col-sm-3 placeholder">
            <div id="canvas-holder">
                <canvas id="wonpiechart" width="200" height="200"/>
            </div>
            <h4>Gewinnverteilung</h4>
            <span class="text-muted">Wer hat wie oft gewonnen?</span>
        </div>
        <div class="col-xs-6 col-sm-3 placeholder">
            <div id="canvas-holder">
                <canvas id="doublepiechart" width="200" height="200"/>
            </div>
            <h4>Doppelte Wertungen</h4>
            <span class="text-muted">Wie oft wurde doppelt gewertet?</span>
        </div>
    </div>

    <h1 class="sub-header">Punkte tabellarisch</h1>
    <div class="table-responsive">
        <table class="table table-striped">
            <thead>
            <tr>
                @foreach ($names as $name)
                    <th>{{ $name }}</th>
                @endforeach
                <th>doppelt</th>
                <th>&nbsp;</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                @foreach ($viewEntity->getSum() as $playersum)
                    <td><b>{{ $playersum }}</b></td>
                @endforeach
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                {!! Form::open(array('action' => 'TableController@showTable', 'method' => 'post')) !!}
                @foreach ($names as $name)
                    <td>{!! Form::text($name, '', array('size' => 3, 'maxlength' => 3)) !!}</td>
                @endforeach
                <td width="100">{!! Form::checkbox('double') !!}</td>
                <td width="100">{!! Form::submit('Eintragen') !!}</td>
                {!! Form::close() !!}
            </tr>
            @foreach ($viewEntity->getPoints() as $key => $row)
            <tr>
                @foreach ($row as $cell)
                    <td>{{ $cell }}</td>
                @endforeach
                @if($viewEntity->getDoubleGames()[$key])
                    <td>ja</td>
                @else
                    <td>nein</td>
                @endif
                <td>&nbsp;</td>
            </tr>
            @endforeach
            </tbody>
        </table>
    </div>
</div>
@endsection

@section('chartjs')
    <script src="js/Chart.js"></script>
    <script>
        var pointPieData = [
            @foreach ($viewEntity->getSum() as $name => $value)
            {
                value: {{ $value }},
                color:"#{{ $colors[$name] }}",
                highlight: "#{{ $colors[$name] }}",
                label: "{{ $name }}"
            },
            @endforeach
        ];
        $(document).ready(function(){
            var pointpiechart = document.getElementById("pointpiechart").getContext("2d");
            window.pointPie = new Chart(pointpiechart).PolarArea(pointPieData);
        });

        var wonPieData = [
            @foreach ($viewEntity->getWonGames() as $name => $value)
            {
                value: {{ $value }},
                color:"#{{ $colors[$name] }}",
                highlight: "#{{ $colors[$name] }}",
                label: "{{ $name }}"
            },
            @endforeach
        ];
        $(document).ready(function(){
            var wonpiechart = document.getElementById("wonpiechart").getContext("2d");
            window.wonPie = new Chart(wonpiechart).Doughnut(wonPieData);
        });

        var doublePieData = [
            {
                value: {{ $viewEntity->getDoubleStatistic()['double'] }},
                color:"#FFC708",
                highlight: "#FFC708",
                label: "doppelt"
            },
            {
                value: {{ $viewEntity->getDoubleStatistic()['single'] }},
                color:"#949FB1",
                highlight: "#949FB1",
                label: "einfach"
            },
        ];
        $(document).ready(function(){
            var doublepiechart = document.getElementById("doublepiechart").getContext("2d");
            window.doublePie = new Chart(doublepiechart).Doughnut(doublePieData);
        });
    </script>
@endsection

@section('navbar')
    <div class="col-sm-3 col-md-2 sidebar">
        <ul class="nav nav-sidebar">
            <li class="active"><a href="#">Aktuelles Spiel <span class="sr-only">(current)</span></a></li>
        </ul>
        <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
        <!-- rommesheet_sidebar_wideskyscraper -->
        <ins class="adsbygoogle"
             style="display:inline-block;width:160px;height:600px"
             data-ad-client="ca-pub-0548666083299896"
             data-ad-slot="3408280051"></ins>
        <script>
            (adsbygoogle = window.adsbygoogle || []).push({});
        </script>
    </div>
@endsection
