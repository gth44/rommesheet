@extends('layouts.master')

@section('content')
    <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
        <h1 class="page-header">Rommesheet</h1>
        {!! Form::open(array('action' => 'TableController@startnames', 'method' => 'post')) !!}
        Spieler 1: {!! Form::text('name[]') !!}<br/>
        Spieler 2: {!! Form::text('name[]') !!}<br/>
        Spieler 3: {!! Form::text('name[]') !!}<br/>
        Spieler 4: {!! Form::text('name[]') !!}<br/>
        Spieler 5: {!! Form::text('name[]') !!}<br/>
        Spieler 6: {!! Form::text('name[]') !!}<br/>
        Spieler 7: {!! Form::text('name[]') !!}<br/>
        Spieler 8: {!! Form::text('name[]') !!}<br/>
        {!! Form::submit('Spielen!') !!}
        {!! Form::close() !!}
    </div>
@endsection

@section('chartjs')
@endsection

@section('navbar')
    <div class="col-sm-3 col-md-2 sidebar">
        <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
        <!-- rommesheet_sidebar_wideskyscraper -->
        <ins class="adsbygoogle"
             style="display:inline-block;width:160px;height:600px"
             data-ad-client="ca-pub-0548666083299896"
             data-ad-slot="3408280051"></ins>
        <script>
            (adsbygoogle = window.adsbygoogle || []).push({});
        </script>
    </div>
@endsection
