<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Http\Request;

class StatisticTest extends TestCase
{
    use WithoutMiddleware;

    /**
     * @var \App\Statistic
     */
    private $statistic;

    /**
     * setup the statistic-class with session, request etc.
     *
     * @return $this
     */
    protected function setupStatistic()
    {
        $this->withoutMiddleware();
        $sessionhandler = new \Symfony\Component\HttpFoundation\Session\Storage\Handler\NullSessionHandler();
        $session = new \Illuminate\Session\Store('laravel_session', $sessionhandler);

        $request = Request::create(
            'http://localhost', 'GET', array(),
            array(), array(), array(), null
        );
        $request->setSession($session);
        $sessionstorage = new \App\Storage\Session($request->getSession());

        $this->statistic = new \App\Statistic($request, $sessionstorage);

        return $this;
    }

    /**
     * check, if the update works correctly on session
     *
     * @return void
     */
    public function testUpdateSession()
    {
        $this->setupStatistic();

        $params = array(
            'User1' => 0,
            'User2' => 20
        );

        $this->statistic->update($params, false);
        $this->assertEquals($this->statistic->getSum()['User2'], 20);
        $this->assertEquals($this->statistic->getSum()['User1'], 0);

        $this->statistic->update($params, false);
        $this->assertEquals($this->statistic->getSum()['User2'], 40);
        $this->assertEquals($this->statistic->getSum()['User1'], 0);

        $this->statistic->update($params, true);
        $this->assertEquals($this->statistic->getSum()['User2'], 80);
        $this->assertEquals($this->statistic->getSum()['User1'], 0);

        $params = array(
            'User1' => 10,
            'User2' => 0
        );
        $this->statistic->update($params, true);
        $this->assertEquals($this->statistic->getSum()['User2'], 80);
        $this->assertEquals($this->statistic->getSum()['User1'], 20);

        // check the number of correctly counted double-games
        $this->assertEquals($this->statistic->getDoubleStatistic()['single'], 2);
        $this->assertEquals($this->statistic->getDoubleStatistic()['double'], 2);

        // check the number of correctly counted the won games per User
        $this->assertEquals($this->statistic->getWonGames()['User1'], 3);
        $this->assertEquals($this->statistic->getWonGames()['User2'], 1);

    }
}
