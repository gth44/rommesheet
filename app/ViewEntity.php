<?php

namespace App;

class ViewEntity
{

    /**
     * @var Statistic
     */
    private $statistic;

    /**
     * ViewEntity constructor.
     *
     * @param Statistic $statistic
     */
    public function __construct(Statistic $statistic)
    {
        $this->statistic = $statistic;
    }

    /**
     * @return array
     */
    public function getDoubleStatistic()
    {
        return $this->statistic->getDoubleStatistic();
    }

    /**
     * @return array
     */
    public function getPoints()
    {
        return $this->statistic->getPoints();
    }

    /**
     * @return array
     */
    public function getWonGames()
    {
        return $this->statistic->getWonGames();
    }

    /**
     * @return array
     */
    public function getDoubleGames()
    {
        return $this->statistic->getDoubleGames();
    }

    /**
     * @return array
     */
    public function getSum()
    {
        return $this->statistic->getSum();
    }


}