<?php

namespace App\Http\Controllers;

use App\Statistic;
use App\Storage\Session;
use App\ViewEntity;
use Illuminate\View\View;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

class TableController extends Controller
{
    /**
     * @var SessionInterface
     */
    private $session;

    /**
     * @var array
     */
    private $playerColour = array();

    /**
     * @var array
     */
    private $playerNames = array();

    /**
     * @param array $players
     *
     * @return array
     */
    protected function generateColour(array $players)
    {
        $colourArray = array(
            'A300E8',
            'F7464A',
            '46BFBD',
            'FDB45C',
            '949FB1',
            '4D5360',
            'FFC708',
            '048EE8'
        );

        if (empty($this->playerColour) === true) {
            $colours = array_slice($colourArray, 0, count($players));
            $this->playerColour = array_combine($players, $colours);
        }

        return $this->playerColour;
    }

    /**
     * Show the profile for the given user.
     *
     * @param Request $request
     *
     * @return View
     */
    public function showTable(Request $request)
    {
        $this->session = $request->session();
        $this->playerNames = $this->session->get('playerNames', array());

        $pointsToAdd = array();
        foreach ($this->playerNames as $playerName)
        {
            $pointsToAdd[$playerName] = $request->get($playerName, 0);
        }
        $double = $request->get('double', false);

        $statistic = new Statistic($request, new Session($request->getSession()));
        $statistic->update($pointsToAdd, (bool)$double);

        $viewEntity = new ViewEntity($statistic);

        $viewVars = array(
            'names'   => $this->playerNames,
            'points'  => $statistic->getPoints(),
            'sum'     => $statistic->getSum(),
            'colors'  => $this->generateColour($this->playerNames),
            'gamesWon' => $statistic->getWonGames(),
            'gamesDouble' => $statistic->getDoubleGames(),
            'gamesDoubleStats' => $statistic->getDoubleStatistic(),
            'viewEntity' => $viewEntity
        );

        return view('showtable', $viewVars);
    }

    /**
     * Show the profile for the given user.
     *
     * @param Request $request
     *
     * @return View
     */
    public function showStartform(Request $request)
    {
        $session = $request->session();
        $session->regenerate(true);
        $session->forget('points');
        $session->forget('payerNames');
        $session->forget('gamesDouble');

        return view('start');
    }

    /**
     * @param Request $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function startnames(Request $request)
    {
        $playerNames = array();
        foreach ($request->get('name') as $name)
        {
            if (empty($name) === false)
            {
                $playerNames[] = $name;
            }
        }
        $request->session()->put('playerNames', $playerNames);

        return redirect()->action('TableController@showTable');
    }
}
