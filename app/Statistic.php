<?php

namespace App;

use Illuminate\Http\Request;

class Statistic
{
    /**
     * @var array
     */
    private $points = array();

    /**
     * @var array
     */
    private $gamesWon = array();

    /**
     * @var array
     */
    private $gamesDouble = array();

    /**
     * @var array
     */
    private $gamesDoubleStatistic = array();

    /**
     * @var array
     */
    private $sum = array();

    /**
     * @var Request
     */
    private $request;

    /**
     * @var Storage\StorageInterface
     */
    private $storage;

    /**
     * Statistic constructor.
     *
     * @param Request                  $request
     * @param Storage\StorageInterface $storage
     */
    public function __construct(Request $request, Storage\StorageInterface $storage)
    {
        $this->request = $request;
        $this->storage = $storage;
    }

    /**
     * @param array $pointsToAdd
     * @param bool $double
     *
     * @return $this
     */
    public function addPoints(array $pointsToAdd, $double = false)
    {
        $sum = 0;
        foreach ($pointsToAdd as $player => $pointValue)
        {
            $this->gamesWon[$player] = 0;
            if ($double === true)
            {
                $pointValue = $pointValue * 2;
                $pointsToAdd[$player] = $pointValue;
            }
            $sum += $pointValue;
        }

        $points = $this->storage->get('points', array());
        $gamesDouble = $this->storage->get('gamesDouble', array());
        if ($sum > 0)
        {
            $points[] = $pointsToAdd;
            $gamesDouble[] = $double;
        }
        $this->storage->set('points', $points);
        $this->storage->set('gamesDouble', $gamesDouble);
        $this->points = $points;
        $this->gamesDouble = $gamesDouble;

        return $this;
    }

    /**
     * @return array
     */
    public function gamesDoubleStatistic()
    {
        $stats = array('double' => 0, 'single' => 0);
        $gamesDouble = $this->storage->get('gamesDouble', array());

        foreach($gamesDouble as $double)
        {
            if ((bool)$double === true)
            {
                $stats['double']++;
            }
            else
            {
                $stats['single']++;
            }
        }
        $this->gamesDoubleStatistic = $stats;
        return $stats;
    }

    /**
     * @param array $pointsList
     *
     * @return array
     */
    public function sum(array $pointsList)
    {
        $sum = array();

        foreach ($pointsList as $pointSets)
        {
            foreach ($pointSets as $player => $roundPoints)
            {
                if (isset($sum[$player]) === false) {
                    $sum[$player] = 0;
                }
                $sum[$player] += $roundPoints;

                if ((int)$roundPoints === 0)
                {
                    $this->gamesWon[$player]++;
                }
            }
        }

        $this->sum = $sum;

        return $sum;
    }

    /**
     * @return array
     */
    public function getDoubleStatistic()
    {
        return $this->gamesDoubleStatistic;
    }

    /**
     * @return array
     */
    public function getPoints()
    {
        return array_reverse($this->points);
    }

    /**
     * @return array
     */
    public function getWonGames()
    {
        return $this->gamesWon;
    }

    /**
     * @return array
     */
    public function getDoubleGames()
    {
        return array_reverse($this->gamesDouble);
    }

    /**
     * @return array
     */
    public function getSum()
    {
        return $this->sum;
    }

    /**
     * @param      $pointsToAdd
     * @param bool $double
     *
     * @return $this
     */
    public function update($pointsToAdd, $double = false)
    {
        $this->addPoints($pointsToAdd, (bool)$double);
        $this->sum = $this->sum($this->getPoints());
        $this->gamesDoubleStatistic = $this->gamesDoubleStatistic();

        return $this;
    }

}