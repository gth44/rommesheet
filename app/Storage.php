<?php

namespace App;

class Storage
{
    /**
     * @var Storage\StorageInterface
     */
    private $storage;

    /**
     * @param Storage\StorageInterface $storage
     */
    public function __construct(Storage\StorageInterface $storage)
    {
        $this->storage = $storage;
    }

    /**
     * @param Storage\StorageInterface $storage
     *
     * @return $this
     */
    public function setStorage(Storage\StorageInterface $storage)
    {
        $this->storage = $storage;

        return $this;
    }

    /**
     * @return $this
     */
    public function newGame()
    {
        // do sth

        return $this;
    }
}