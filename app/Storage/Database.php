<?php

namespace App\Storage;

use Illuminate\Support\Facades\DB;

class Database implements StorageInterface
{
    /**
     * this is an id - in fact it is the sessionId we use
     *
     * @var string
     */
    private $gameNumber = 0;

    /**
     * @var \PDO
     */
    private $database;

    public function __construct($gameNumber)
    {
        $this->gameNumber = $gameNumber;
        $this->database = DB::connection()->getPdo();
    }

    public function get($name, $default = null)
    {
        // TODO: Implement get() method.
    }

    /**
     * @param $name
     * @param $value
     */
    public function set($name, $value)
    {
        // TODO: Implement set() method.
    }

    /**
     * @param $name
     */
    public function forget($name)
    {
        // TODO: Implement forget() method.
    }

    /**
     * @param $gameNumber
     *
     * @return $this
     */
    public function setGameNumber($gameNumber)
    {
        $this->gameNumber = $gameNumber;

        return $this;
    }
}