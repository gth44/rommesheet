<?php

namespace App\Storage;

interface StorageInterface
{
    public function get($name, $default = null);

    /**
     * @param $name
     * @param $value
     */
    public function set($name, $value);

    /**
     * @param $name
     */
    public function forget($name);

}