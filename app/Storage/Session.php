<?php

namespace App\Storage;

/**
 * Class Session
 * This class is the abstraction layer to write/read within the SESSION.
 *
 * @package App\Storage
 */
class Session implements StorageInterface
{
    /**
     * @var \Illuminate\Session\Store
     */
    private $storage;

    public function __construct($session)
    {
        $this->storage = $session;
    }

    /**
     * @param      $name
     * @param null $default
     *
     * @return mixed
     */
    public function get($name, $default = null)
    {
        return $this->storage->get($name, $default);
    }

    /**
     * @param $name
     * @param $value
     */
    public function set($name, $value)
    {
        $this->storage->put($name, $value);
    }

    /**
     * @param $name
     */
    public function forget($name)
    {
        $this->storage->forget($name);
    }

}